#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 16 09:35:42 2019

@author: rgb
"""

import nazca as nd

with nd.Cell(name='test') as C:
    nd.Pin(name='a0', width=2.0).put()

p = C.pin['a0']
print(p.name)
print(p.width)
#'a0'
#'2.0'