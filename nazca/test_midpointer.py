#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 16 08:14:53 2019

@author: rgb
"""

import nazca as nd

p1 = nd.Pin().put(10, 20, 0)
p2 = nd.Pin().put(20, 40, 90)

p3 = nd.midpointer(p1, p2)
print(p3.xya())

x, y, a = nd.midpoint(p1, p2)
print(x, y, a)