#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
**IMEC PDK xsectional structures and epi.**

This module is intended to define the xsectional structure of the waveguides of a technology.
The structure can be defined using horizontal and vertical layers stacks of the materials defined in
pdk_10_materials.py.
"""


import nazca as nd
from . import pdk_10_materials as mat

#Introduce xsection objects. Provide a string name for future referencing.
xsFC = nd.add_xsection('FC')
xsWG = nd.add_xsection('WG')

#==============================================================================
# 2D waveguide xsections
# Define waveguide xsections via the Xsection methods.
# A xsection description can be used in visualisation or input to mode solvers.
#==============================================================================
wguide = 3.0
hfilm = 0.22
hsub = 1.0
hclad = 1.0
FC_etch = 0.07
epi = [(mat.SiO2,hsub), (mat.Si,hfilm), (mat.SiO2,hclad)]

#Shallow guide
xsFC.layers = epi
xsFC.background = mat.SiO2
Ls1 = xsFC.add_vstack(name='backgrnd', etchdepth=hclad+FC_etch)
Ls2 = xsFC.add_vstack(name='ridge')
xsFC.add_hstack(name='guide', layers=[(Ls1,1.0), (Ls2,wguide), (Ls1,1.0)])

#Deep guide
xsWG.layers = epi
xsWG.background = mat.SiO2
Ld1 = xsWG.add_vstack(name='backgrnd', etchdepth=hclad+hfilm)
Ld2 = xsWG.add_vstack(name='ridge')
xsWG.add_hstack(name='guide', layers=[(Ld1,1.0), (Ld2,wguide), (Ld1,1.0)])
