#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
# This file is part of Nazca.
#
# Nazca is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# Nazca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Nazca.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------
#
#@Authors: Katarzyna Lawniczuk, Ronald Broeke
#@email: nazca@brightphotonics.eu
#2017(c)

"""
IMEC PDK initialization.
"""


from nazca import cfg
from nazca.IMEC_tech.pdk_10_materials import *
from nazca.IMEC_tech.pdk_15_xsections import *
from nazca.IMEC_tech.pdk_20_technology import *
from nazca.IMEC_tech.pdk_30_BB_library import *
from nazca.IMEC_tech.pdk_40_project_templates import *
cfg.reset_pin_settings()