
"""
**IMEC PDK material settings.**

This module is intended to define the material models and materials used in the IMEC_tech.
"""

import nazca as nd


#==============================================================================
# Materials
#==============================================================================

#Define materials. Use value or functions to set the refractive index:
Si  = nd.xsection.Material(Nmat=3.47, name='Si',  rgb=(0.0, 0.4, 0.9))
SiO2 = nd.xsection.Material(Nmat=1.44,   name='SiO2', rgb=(0.0, 0.8, 0.3))
Air = nd.xsection.Material(Nmat=1.0,   name='Air', rgb=(0.3, 0.3, 0.3))

