
"""
**IMEC PDK technology settings.**

This module is intended to define the mask layers and xsections of a technology,
which can be accomplished by loading the coresponding csv tables with those
settings and/or use the nazca function that add layers and sections.

The **Xsection objects** are defined in this module. A xsection corresponds typically
to a waveguide or metal line. The following properies of the xsection can be set

    * os: straight-to-bend waveguide offset
    * width: default waveguide width
    * radius: default bend radius
    * taper: taper length
"""



import os
import nazca as nd
from math import copysign

# full path of the (demo) directory that has __init__ file.
dir_path = os.path.dirname(__file__)
gds_path = os.path.join(dir_path, 'gdsBB/')


#==============================================================================
# Load Demo foundry layers from tables store as csv files.
#==============================================================================
layer_file           = os.path.join(dir_path, 'table_layers.csv')
xsection_layer_file  = os.path.join(dir_path, 'table_xsection_layer_map.csv')
xsection_file        = os.path.join(dir_path, 'table_xsections.csv')
layercolor_file      = os.path.join(dir_path, 'table_colors_Light.csv')

#load the foundry tables
nd.load_layers(filename=layer_file)
nd.load_xsections(filename=xsection_file)
nd.load_xsection_layer_map(filename=xsection_layer_file)
nd.load_layercolors(filename=layercolor_file)


nazca_logo_layers = {'ring':1, 'box':None, 'bird':11}
nazca_logo = nd.nazca_logo(nazca_logo_layers)



#==============================================================================
# xsection attributes
#==============================================================================
xsFC = nd.add_xsection('FC')
xsFC.os = 0.0
xsFC.width = 3.0
xsFC.radius = 200.0
xsFC.minimum_radius = 5.0
xsFC.taper = 50.0

xsWG = nd.add_xsection('WG')
xsWG.os = 0.0
xsWG.width = 1.5
xsWG.radius = 20.0
xsWG.minimum_radius = 5.0
xsWG.taper = 50.0


