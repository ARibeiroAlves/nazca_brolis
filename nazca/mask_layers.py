#!/usr/bin/env python3
#-----------------------------------------------------------------------
# This file is part of Nazca.
#
# Nazca is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# Nazca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Nazca.  If not, see <http://www.gnu.org/licenses/>.
#
# @author: Ronald Broeke (c) 2016-2019
# @email: ronald.broeke@brightphotonics.eu
#-----------------------------------------------------------------------
# -*- coding: utf-8 -*-
"""
This module defines
1- layers in DataFrame 'layer_table'
2- xsections in DataFrame 'xsection_table.
3- xsection layers in DataFrame 'xsection_layer table', adding layers to each 'xs'
4- The 'layer_table' and 'xsection_layer table' are joined using the 'layer2xsection table'.

The result is stored in a dictionary containing the xsection-layer information
per 'xs' needed in layout: 'xsection_layers'

Layers and xsection can be added in two ways
1 - Loading from csv file: load_layers, load_xs
2 - Functions add_layer, add_xsection
When adding layers to a 'xs' the xs_map is built automatically.

It is also possible to first load layers and xs, and then extend them via
'add_layer' and/or 'add_layer2xsection'.
"""

from collections import defaultdict
import pandas as pd
import matplotlib as mpl
from matplotlib.colors import rgb2hex
from . import cfg
from . import xsection
from pprint import pprint


# keep track of layers used by the designer that have not been defined:
unknown_layers = set()
unknown_xsections = set()
#cfg.layerset = set() #set of all (L, D) layers defined

cfg.layer_names = {}
cfg.layer_LDT = {}
cfg.layer_LDT_count = defaultdict(int)

cfg.xsection_table = pd.DataFrame()
xsection_table_attr = ['xsection', 'xsection_foundry', 'origin', 'stub']


#layer-table:
cfg.layer_table = pd.DataFrame() # layer_name -> index
layer_table_attr_csv  = ['layer', 'datatype', 'tech', 'layer_name_foundry', 'accuracy', 'origin',       'remark']
#rename attributes for the xs-layer join to avoid name clashes:
layer_table_attr_join = ['layer', 'datatype', 'tech', 'layer_name_foundry', 'accuracy', 'origin_layer', 'remark']


#layercolor table:
cfg.colors = pd.DataFrame()
cfg.xs_list = []
colors_attr_csv = ['depth', 'layer', 'datatype', 'name', 'fill_color', 'frame_color', 'frame_brightness', 'fill_brightness', 'dither_pattern', 'valid', 'visible', 'transparent', 'width', 'marked', 'animation']


#xsection-layer-map table, mapping layers to xsections:
cfg.xsection_layer_map = pd.DataFrame()

xsection_layer_map_attr_csv  = ['layer_name', 'xsection', 'growx', 'growy', 'origin']
#rename attributes for the xs-layer join to avoid name clashes:
xsection_layer_map_attr_join = ['layer_name', 'xsection', 'growx', 'growy', 'origin_xs']

xsection_table_attr

#final set of export attributes for joined xsection-layers table:
mask_layers_attr = ['layer_name', 'xsection', 'layer', 'datatype', 'tech', 'growx', 'growy', 'accuracy']


layer_debug = False # flag for debug output to stdout.


def _check_duplicates(df, tablename):
    """Find duplicated layers entries in a layer table.

    Duplicates looks for ['layer', 'datatype', 'tech'].

    Return:
        bool: True if duplicate layers are found.
    """
    ID = ['layer', 'datatype', 'tech']
    G = df.duplicated(subset=ID, keep=False)
    if G.any():
        if layer_debug:
            print("\nDuplicate layers found in '{}':".format(tablename))
            print("\nSame ['layer', 'datatype', 'tech'], but different layer_name.")
            print(df[['layer_name']+ID].sort_values(by=ID)[G])
        return True
    return False


def add_layer(
    name=None,
    layer=None,
    tech=None,
    accuracy=None,
    fab_name=None,
    origin=None,
    remark=None,
    # next color info:
    frame_color=None,
    fill_color=None,
    frame_brightness=None,
    fill_brightness=None,
    dither_pattern=None,
    valid=None,
    visible=None,
    transparent=None,
    width=None,
    marked=None,
    animation=None,
    alpha=None,
    unknown=False,
    merge=True):
    """Create a new mask layer.

    Args:
        name (str): layer name
        layer (int | tuple): layer number or (layer, datatype)
        tech (str): technology ID
        accuracy (float): mask resolution of the layer in um
        fab_name (str): optional name used by the foundry for this layer
        origin (str): who created the layer
        remark (str): extra info for the layer
        unknown (bool): True if redirected from get_layer (default = False)
        merge (bool): if True merge xscetion and layers (default is True)
        ...: color attributes

    Returns:
        DataFrame: Table of mask layers
    """
    if unknown:
        # redirected from get_layer -> layer unknown.
        # if the layer corresponds to a default_layer in cfg
        # create it here as a an actual layer.
        isDefault = False
        if name in cfg.default_layers.keys():
            LDT = cfg.default_layers[name]
            if len(LDT) == 2:
                _layer, _datatype = LDT
                tech = cfg.default_tech
            else:
                _layer, _datatype, _tech = LDT
            isDefault = True
        elif layer is None:
            return get_layer('dump')
        else:
            _layer, _datatype, tech = layer
            newML = (_layer, _datatype)
            if newML in cfg.default_layers.values():
                for MLname, ML in cfg.default_layers.items():
                    if ML == newML:
                        name = MLname
                        isDefault = True
                        break
        if not isDefault:
            #create a new layer or redirect to 'dump'
            if cfg.redirect_unknown_layers:
                print("Unknown layer '{}' or layer numbers {} in get_layer."\
                    " Moving layout content to the dump layer {} instead."\
                    " ('redirect_unknown_layers' is True). ".\
                        format(name, (_layer, _datatype, tech), 'dump'))
                return get_layer('dump')

    #   assert isinstance(_layer, int)
    #   assert isinstance(_datatype, int)
    else:
        if tech is None:
            tech = cfg.default_tech
        if isinstance(layer, tuple):
            if len(layer) == 2:
                _layer, _datatype = layer[0], layer[1]
            elif len(layer) == 3:
                _layer, _datatype, tech = layer[0], layer[1], layer[2]
            else:
                raise Exception("Invalid layer value: {}".format(layer))
        else:
            _layer = layer
            _datatype = 0
    if _layer is None:
        raise Exception("Error: no layer number provided in call to add_layer().")
    elif _layer != int(_layer):
        raise Exception("layer value '{}' should be of type integer".format(_layer))
    if _datatype != int(_datatype):
        raise Exception("datatype value '{}' should be of type integer".format(_datatype))

    if accuracy is None:
        accuracy = cfg.default_xs['accuracy'][0]
    if origin is None:
        origin = cfg.default_xs['origin'][0]
    if name is None:
        name = '{}/{}/{}'.format(_layer, _datatype, tech)

    if name in cfg.layer_names.keys(): #existing layer_name (ID):
        raise Exception("Warning: reusing an existing layer_name '{}'."\
            " Use a different layer name."\
            .format(name))
    else:
        LDT = (_layer, _datatype, tech)
        cfg.layer_names[name] = LDT
        cfg.layer_LDT_count[LDT] += 1
        if cfg.layer_LDT_count[LDT] == 1:
            cfg.layer_LDT[LDT] = name

        newlayer = {
            'layer_name': str(name),
            'layer': [_layer],
            'datatype': [_datatype],
            'tech': tech,
            'accuracy': [accuracy],
            'layer_name_foundry': fab_name,
            'origin': [origin],
            'remark': remark,
            'source': 'add_layer'}

        df = pd.DataFrame(newlayer)
        df.set_index('layer_name', inplace=True)
        try: #pandas in python 3.7
            cfg.layer_table = pd.concat([cfg.layer_table, df], sort=True)
        except:  #pandas in python <= 3.6
            cfg.layer_table = pd.concat([cfg.layer_table, df])

    set_layercolor(name, (_layer, _datatype, tech),
        frame_color, fill_color, frame_brightness, fill_brightness,
        dither_pattern, valid, visible, transparent, width, marked, animation,
        alpha)

    merge_xsection_layers_with_layers()
    return name


def add_XSdict(name, name_foundry=None, origin='user', stub=None):
    """Create a new Xsection object named <name>.

    If a Xsection with <name> already exists, the existing Xsection is returned.

    Args:
        name (str): xsection name.
        name_foundry (str): optional xsection name used by the foundry
        origin (str): source/creator of the layer, e.g. 'user', 'foundry' or any string.
        stub: xsection name to be used for the stub of this xsection

    Returns:
        Xsection: Xsection object with name <name>
    """
    if name not in cfg.XSdict.keys():
        cfg.XSdict[name] = xsection.Xsection(name=name)

        # add new info to xsection_layers:
        D = {
            'xsection': name,
            'xsection_foundry': name_foundry,
            'origin': origin,
            'stub': stub
        }
        cfg.xsection_table = cfg.xsection_table.append(D, ignore_index=True)

        #update mask_layers:
        merge_xsection_layers_with_layers()
    return cfg.XSdict[name]

add_xsection = add_XSdict


def add_layer2xsection(xsection='name', layer=None, growx=None,
        growy=None, accuracy=None, fab_name=None, origin=None, remark=None):
    """Add a layer to the Xsection object with name <xsection>.

    If <xsection> does not exist yet it will be created.
    The layer entry is described by (layer, datatype).
    If the (layer, datatype) does not exist yet it will be created.
    The keyword 'accuracy' is only processed in case of a new layer.

    Args:
        xsection (str): xsection name for use in nazca
        layer (int | tuple): layer number or (layer, datatype)
        growx (float): growth in x-direction of polygon in the (layer, datatype)
            (default = 0.0)
        growy (float): growth in y-direction of polygon in the (layer, datatype)
            (default = 0.0)
        accurary (float): accuracy of the grid in um of the (layer, datatype)
            (default = 0.001)
        fab_name (str): technology xsection name, e.g. the foundry name for the layer
        origin (str): source/creator of the layer, e.g. 'user', 'foundry'
        remark (str): extra info about the layer

    Returns:
        Xsection: Xsection object having name <xsection>
    """
    layer = get_layer(layer)
    if growx is None:
        growx = cfg.default_xs['growx'][0]
    if growy is None:
        growy = cfg.default_xs['growy'][0]
    if origin is None:
        origin = cfg.default_xs['origin'][0]
    if accuracy is None:
        accuracy = cfg.layer_table.loc[layer]['accuracy']
    if xsection not in cfg.XSdict.keys():
        add_xsection(name=xsection)

    if not cfg.xsection_layer_map.empty:
        select = (cfg.xsection_layer_map['xsection']==xsection) &\
            (cfg.xsection_layer_map['layer_name']==layer)
        LayerRow = cfg.xsection_layer_map[select]
        if not LayerRow.empty:
            cfg.xsection_layer_map.loc[select, ['growx', 'growy']] = growx, growy
            cfg.xs_list = cfg.xsection_layer_map['xsection'].unique()
            merge_xsection_layers_with_layers()
            return cfg.XSdict[xsection]
    else:
        cfg.default_xs_name = xsection # set default xs to first user defined xs.

    #define new layer entry
    D = {
        'layer_name': str(layer),
        'xsection': xsection,
        'xsection_foundry': fab_name,
        'growx': growx,
        'growy': growy,
        'accuracy': accuracy,
        'origin': origin,
        'remark': remark,
        'source': 'add_layer'
    }
    cfg.xsection_layer_map = cfg.xsection_layer_map.append(D, ignore_index=True)

    merge_xsection_layers_with_layers()
    return cfg.XSdict[xsection]

add_xs = add_layer2xsection
add_xsection_layer = add_layer2xsection


def load_layers(filename, tech=None, clear=False, autocolor=False):
    """Load layer definitions from csv file into the Nazca layer table.

    The load can replace existing layer definitions or extend them (default).

    Args:
        filename (str): name of layer definition file in csv format
        tech (str): technology name
        clear (bool): clear existing layers (default = False)
        autocolor (bool): Create automated color settings per layer (default = False)

    Returns:
        DataFrame: table with layers
    """
    if tech is None:
        tech = cfg.default_tech
    load_table = pd.read_csv(filename, delimiter=',')
    if 'tech' not in load_table:
        load_table['tech'] = tech
        if layer_debug:
            print("Column 'tech' missing in file '{}'."\
                " Column has been added after load."\
                " For explicitly use of 'tech' add it to the table".format(filename))
    load_table.dropna(subset=['layer_name'], inplace=True)
    load_table['layer'] = load_table['layer'].astype(int)
    load_table['datatype'] = load_table['datatype'].astype(int)
    load_table['source'] = filename

    if clear:
        clear_layers()
    for i, row in load_table.iterrows():
        add_layer(
            name=row.layer_name,
            layer=(row.layer, row.datatype),
            accuracy=row.accuracy,
            origin=row.origin,
            remark=row.remark,
            merge=False)
            # merge only after adding multiple layers to save time
            # add_layer() check if the layername is unique
    merge_xsection_layers_with_layers()

    if autocolor:
        for i, (name, L, D, T) in load_table[['layer_name', 'layer', 'datatype', 'tech']].iterrows():
            set_layercolor(name, (L, D, T),
                frame_color=None, fill_color=None, frame_brightness=None,
                fill_brightness=None, dither_pattern=None, valid=None,
                visible=None, transparent=None, width=None, marked=None,
                animation=None, alpha=None)

    return cfg.layer_table


def load_xsection_layer_map(filename, tech=None):
    """Load the assignment of layers to xsections from <filename>.

    Layers or xsections in the file that do not yet exist are created automatically.

    Args:
        filename (str): xsection file in csv format
        tech (str): technology ID.

    Returns:
        DataFrame: merged xsections and layers.
    """
    if tech is None:
        tech = cfg.default_tech
    cfg.xsection_layer_map = pd.read_csv(filename, delimiter=',')
    cfg.xsection_layer_map.dropna(inplace=True)

    if 'tech' not in cfg.xsection_layer_map.columns:
        cfg.xsection_layer_map['tech'] = tech
        if layer_debug:
            print("Column 'tech' missing in file '{}'."\
                "Column has been added after load"\
                " with value {}".format(filename, tech))

    #add xsections in the map to the xsection_table if missing:
    for name in cfg.xsection_layer_map['xsection']:
        add_XSdict(name)

    return merge_xsection_layers_with_layers()


def load_xsections(filename):
    """Load list of xsection from file with stub mapping.

    In addition create a stub map dictionary of a xsection to its stub-xsection.

    Args:
        filename (str): xsection map filename in csv format

    Returns:
        DataFrame: table with loaded xsections
    """
    cfg.xsection_table = pd.read_csv(filename, delimiter=',')
    for name in cfg.xsection_table['xsection']:
        add_XSdict(name)

    temptable = cfg.xsection_table.set_index('xsection')
    cfg.stubmap = temptable['stub'].dropna().to_dict()

    #TODO: check naming consistency of stubs
    #TODO: check for unique names
    #TODO: fill missing nazca_name with fab_name?
    #TODO: check if all xs are present in the xs definition.

    #update mask_layers:
    merge_xsection_layers_with_layers()
    return cfg.xsection_table


def add_xsection_stub(xsection, stub):
    """Assign a stub xsection to a xsection.

    The default stub is the xsection itself.
    """
    cfg.stubmap[xsection] = stub


def load_bbmap(filename):
    """Load the mapping table for nazca to spt.

    The spt format is used for third party OptoDesigner mask assembly.

    Args:
        filename (str): name of file containing bb-mapping in csv format

    Returns:
        None
    """
    bbmaptable = pd.read_csv(filename, delimiter=',')
    bbmaptable.dropna(how='all', inplace=True)
    columns = ['Nazca', 'OD_BBname', 'OD_port', 'odx', 'ody', 'oda', 'x', 'y', 'a']
    cfg.bbmap = bbmaptable[columns]
    cfg.spt = True
    return None


def merge_xsection_layers_with_layers():
    """Create a dictionary containing tables of xsections joined with layers.

    The xsection names are the dictionary keys.
    The dictionary contains all attributes for mask export.

    Left-join xsection table with layer table on (layer, datatype) and
    chop it up into a dictionay with xsection as key and the join result as
    values.

    Returns:
        None
    """
    if cfg.layer_table.empty or\
        cfg.xsection_layer_map.empty or\
        cfg.xsection_table.empty:
        return None

    #TODO: check if column names exists in cfg.xsection_layer_map.empty
    #TODO: deal with NaN entries, i.e. no (layer, datatype) match.

    # rename to internal column names and create layer-xsection merge.
    layers = cfg.layer_table[layer_table_attr_csv].rename(
        columns=dict(zip(layer_table_attr_csv, layer_table_attr_join)))
    layers2 = layers.reset_index()
    xsections = cfg.xsection_layer_map[xsection_layer_map_attr_csv].rename(
        columns=dict(zip(xsection_layer_map_attr_csv, xsection_layer_map_attr_join)))

    Merged = pd.merge(left=xsections, right=layers2, how='left',
        on=['layer_name'])

    #reset the table when merge is called for a fresh build.
    cfg.xs_list = cfg.xsection_table['xsection'].unique()
    for xs in cfg.xs_list:
        rows = Merged['xsection'] == xs
        xs_layer_table = Merged[rows][mask_layers_attr]
        add_xsection(xs).mask_layers = xs_layer_table.set_index('layer_name')
    return None


def load_masklayers(layer_file=None, xsection_layer_file=None):
    """Load layer and xsection files and merge them.

    This function combines
    1. load_layers()
    2. load_xsection_layer_map()
    3. merge()

    Args:
        layer_file (str): layer file name of csv file
        xsection_layer_file (str): xsection file name of csv file

    Returns:
        dict: {xsection name: DataFrame with mask layer}
    """
    load_layers(filename=layer_file)
    load_xsection_layer_map(filename=xsection_layer_file)
    return merge_xsection_layers_with_layers()


def get_xsection(name):
    """Return the Xsection object corresponding to <name>.

    Args:
        name (str | Node): xsection name or a pin object

    Returns:
        Xsection: Xsection object with name <name>
    """

    try: # if name is a pin (Node) get its xsection name
        name = name.xs
    except:
        pass
    if not name in cfg.XSdict.keys():
        msg = "\nNo xsection object existing under xsection name '{}'.".format(name)
        msg += ' Available xsections are:'
        for xname in cfg.XSdict.keys():
            msg += "\n  '{}'".format(xname)
        msg += "\nAlternatively, add a new xsection as:"
        msg += "\n  add_xsection(name='{}')".format(name)
        raise Exception(msg)

    return cfg.XSdict[name]

#getXS = get_xsection


def get_layer(layer):
    """Get the layer ID for a specific layer reference.

    If the <layername> does not exist a default layer ID is returned.

    Args:
        layer (str | tuple | int): layer reference by name | (layer, datatype) | layer

    Returns:
        tuple: layer_ID (layer_name)
    """
    if isinstance(layer, str) or layer is None:
        if layer in cfg.layer_names.keys():
            return layer
        else:
            return add_layer(name=layer, layer=None, unknown=True)

    # parse LDT:
    elif isinstance(layer, tuple):
        n = len(layer)
        if n == 3:
            LDT = layer
        elif n == 2:
            LDT = (layer[0], layer[1], cfg.default_tech)
        elif n ==1:
            LDT = (layer[0], 0, cfg.default_tech)
        else:
            raise("Error: invalid tuple format for layer {}. Tuple length must be <= 3.".\
                format(layer))

    elif isinstance(layer, int):
        LDT = (layer, 0, cfg.default_tech)

    status = cfg.layer_LDT_count.get(LDT, 0)
    if status == 1: #unique mapping possible
        return cfg.layer_LDT[LDT]
    if status > 1:
        if LDT not in cfg.mapto_layer_name.keys():
            names = [name for name, ldt in cfg.layer_names.items() if ldt == LDT]
            cfg.mapto_layer_name[LDT] = names[0]
            print("WARNING: Non-unique layer identifier used: {0}.\n"\
                "Matching layer_names for {0} are {1}.\n"\
                "Continuing here with layer_name = '{2}'.".\
                format(layer, names, cfg.mapto_layer_name[LDT]))
                # Note that a non-unique (L, D) becomes tricky when importing GDS;
                # The GDS format only uses (L, D) as the layer identifier, and this has to
                # be mapped to a unique layer_name. The designer has to assign
                # that mapping: (L, D) -> layer_name to resolve conflicts.
                # As a default the first (L, D) defined can be used.
                # Issue this warning only once in stdout, or move to a log-file.
        return cfg.mapto_layer_name[LDT]

    # if we get here, then the layer has not been not found yet:
    if cfg.redirect_unknown_layers:
        print("Unknown layer '{}' in get_layer moved to dump layer {} instead."
            " ('redirect_unknown_layers' is True). ".\
                format(LDT, cfg.default_layers['dump' ]))
        try:
            cfg.layer_names['dump']
        except:
            add_layer(name='dump', layer=cfg.default_layers['dump'] )
        return 'dump'
    else:
        return add_layer(name=None, layer=LDT, unknown=True)


def clear_layers():
    """Clear all objects with layer information.

    Returns:
        None
    """
    global unknown_layers
    unknown_layers = set()
    cfg.layer_names = {}
    cfg.layer_LDT = {}
    cfg.layer_LDT_count = defaultdict(int)
    cfg.layer_table.drop(cfg.layer_table.index, inplace=True)
    cfg.xsection_layer_map.drop(cfg.xsection_layer_map.index, inplace=True)
    cfg.colors.drop(cfg.colors.index, inplace=True)
    for xs in cfg.xs_list:
        get_xsection(xs).mask_layers = dict()
    cfg.xs_list = []


def clear_xsections():
    """Drop all rows from layers and xs tables.

    Returns:
        None
    """
    cfg.xsection_layer_map.drop(cfg.xsection_layer_map.index, inplace=True)
    #cfg.mask_layers = dict()
    for xs in cfg.xs_list:
        get_xsection(xs).mask_layers = dict()
    cfg.xs_list = []


def empty_xsections():
    """Delete info on all xs. Keep layer info.

    Returns:
        None
    """
    cfg.xsection_layer_map = pd.DataFrame()
    merge_xsection_layers_with_layers()


def load_parameters(filename):
    """Obsolete"""
    cfg.dfp = pd.read_csv(filename, delimiter=',')


def get_parameter(name):
    """Obsolete.

    Set parameter value for specific name.
    """
    return float(cfg.dfp[cfg.dfp['name'] == name]['value'])


#==============================================================================
# plt
#==============================================================================
def set_plt_properties(figsize=14, cmap=None, N=32, alpha=0.3):
    """Set the default colormap to use in Matplotlib output for mask layout.

    Args:
        figsize (float): size of the matplotlib figure (default = 14)
        cmap (str): name of the colormap, (default = None uses cfg.plt_cmap_name)
        N (int): the number of colors in the map in case of a 'linearSegmentedColormap' default = 32)
        alpha (float): transparency of the cmap to see through layers (default = 0.3).

    Returns:
        None
    """

    if cmap is None:
        cmap = cfg.plt_cmap_name

    mp = mpl.cm.get_cmap(cmap)
    if isinstance(mp, mpl.colors.LinearSegmentedColormap):
        cfg.plt_cmap = []
        for n in range(N):
            cfg.plt_cmap.append(mp(n/N))
    else:
        cfg.plt_cmap = mp.colors

    cfg.plt_cmap_size = len(cfg.plt_cmap)
    cfg.plt_figsize = figsize
    cfg.plt_alpha = alpha

#make sure colors are set:
set_plt_properties()


#==============================================================================
# colors
#==============================================================================
color_defaults = {
    'frame_brightness': 0,
    'fill_brightness': 0,
    'fill_color': 0,
    'frame_color': 0,
    'dither_pattern': 'I0',
    'valid': True,
    'visible': True,
    'transparent': True,
    'width': 0,
    'marked': False,
    'animation': 1,
    'alpha' : 0.3
}


def load_layercolors(filename):
    """Read colormap from a .csv Nazca color table file.

    Args:
        filename (str): filename

    Returns:
        None
    """

    #For Klayout tabs with groups the layer column contains a '*'
    #and Pandas will create type->str
    #For tabs without groups the layer is read as type->int
    df1 = pd.read_csv(filename)
    df1['layer'] = df1['layer'].astype(str)
    #df1.loc[df1['width'] == 'na', 'width'] = 0
    df1.replace('na', '', inplace=True)

    #TODO: creates a brand new table and forgets present settings
    #  needs an update to handle mutliple layer/color sets.
    cfg.colors = df1[df1['layer'] != '*'].dropna(subset=['layer'])
    cfg.colors.reset_index(inplace=True)
    try:
        cfg.colors['layer'] = cfg.colors['layer'].astype(int)
        cfg.colors['datatype'] = cfg.colors['datatype'].astype(int)
    except:
        print('Warning: Check load_layercolors.')
        #print('Group in load_layercolors:', cfg.colors['layer'], '\n')
    #TODO: the try will trigger on any layer entry with text.
    # as a resuls all the layers and datatype remain a string.

    cfg.colors['alpha'] = 0.3
    return None


# note: fill_color and frame_color will come from default colormap.
def set_layercolor(
    name='unknown',
    layer=None,
    frame_color=None,
    fill_color=None,
    frame_brightness=None,
    fill_brightness=None,
    dither_pattern=None,
    valid=None,
    visible=None,
    transparent=None,
    width=None,
    marked=None,
    animation=None,
    alpha=None):
    """Set layer color information.

    Generate a tabel (DataFrame) with layer color information.
    For missing attribute values for fill_color and frame_color,
    the default_colors as specified in colormap cfg.plt_cmap are be applied.
    Note that the default colormap can be adjusted by the user.

    Args:
        name (str): layername
        layer (int | tuple): layer number or (layer, datatype)
        tech (str) : technology (default = None)
        ...: attributes

    Returns:
        None
    """
    if layer is None:
        raise ValueError('Need to provide a layer number, or name to set layer colors.')
    else:
        layer, datatype, tech = cfg.layer_names[get_layer(name)]
    name = "{}/{} {}".format(layer, datatype, name)

    colors = {
        'depth': 1,
        'name': name,
        'layer': str(layer),
        'datatype': str(datatype),
        'fill_color': fill_color,
        'frame_color': frame_color,
        'frame_brightness': frame_brightness,
        'fill_brightness': fill_brightness,
        'dither_pattern': dither_pattern,
        'valid': valid,
        'visible': visible,
        'transparent': transparent,
        'width': width,
        'marked': marked,
        'animation': animation,
        'alpha' : alpha
    }

    if not cfg.colors.empty:
        mask = cfg.colors['layer'] == int(layer)
        mask2 = cfg.colors['datatype'] == int(datatype)
        select = cfg.colors.loc[mask & mask2]
        coloritems = len(select)
    else:
        coloritems = 0

    if coloritems == 0:
        #TODO: not here or the colors can't be changed later on.
        #col = mpl.colors.to_hex(cfg.plt_cmap[layer % cfg.plt_cmap_size])
        col = rgb2hex(cfg.plt_cmap[int(layer % cfg.plt_cmap_size)])

        if fill_color is None:
            colors['fill_color'] = col
        if frame_color is None:
            colors['frame_color'] = col
        for attr in colors.keys():
            if colors[attr] is None:
                if attr in color_defaults.keys():
                    colors[attr] = color_defaults[attr]

        df = pd.DataFrame(colors, index=[0])
        try: #pandas in python 3.7
            cfg.colors = pd.concat([cfg.colors, df], ignore_index=True, sort=True)
        except:  #pandas in python <= 3.6
            cfg.colors = pd.concat([cfg.colors, df], ignore_index=True)

        cfg.colors['layer'] = cfg.colors['layer'].astype(str)
        cfg.colors['datatype'] = cfg.colors['datatype'].astype(str)
    elif coloritems == 1:
        for attr in cfg.colors.columns: #colors.keys(): #use existing values for undefined attributes
            if attr in colors.keys():
                if colors[attr] is None:
                    #TODO: Better use a try to catch color_defaults are missing:
                    if attr in color_defaults.keys():
                        colors[attr] = select[attr].iloc[0]
            else:
                colors[attr] = select[attr].iloc[0]
        df = pd.DataFrame(colors, index=select.index) #.set_index(idx)

        # TODO: this could be a new (extra) layername with an old layer number
        # now it's always overwritten when add_layer() is called:
        cfg.colors.loc[mask & mask2] = df
    else:
        print('Multiple color entries for ML({}, {}). Skipping...'.format(layer, datatype))

    return None


def save_colormap(filename=None):
    """Save a colormap to csv file.

    Args:
        filename (str): output filename

    Returns:
        None
    """
    if filename is None:
        filename = "colors_saved.csv"
    cfg.colors.reset_index()
    bools = ['valid', 'visible', 'transparent', 'marked']
    colors = cfg.colors.copy()
    for colm in bools:
        mask = colors.loc[:, colm] == True
        colors.loc[mask, colm] = 'true'
        colors.loc[~mask, colm] = 'false'
    colors[colors_attr_csv].to_csv(path_or_buf=filename, sep=',')
    return None


#==============================================================================
# Show or get tables
#==============================================================================
def get_layers():
    """Get predefined selection of columns from layer_table DataFrame.

    Returns:
        DataFrame: layer info ['nazca_name', 'layer', 'datatype', 'tech', 'accuracy']
    """
    df = cfg.layer_table[['nazca_name', 'layer', 'datatype', 'tech', 'accuracy']]
    return df

def show_xsections():
    """Print the xsections table.

    Returns:
        None
    """
    #print('-------------------------------')
    print('xsections:')
    if not cfg.xsection_table.empty:
        pprint(cfg.xsection_table[xsection_table_attr])
    else:
        print('No xsections defined.')

def show_layers():
    """Print the layer table."""
    #print('-------------------------------')
    #print("DataFrame 'cfg.layer_table' with layer definitions:")
    print("layers:")
    try:
        #df = cfg.layer_table[['layer_name', 'layer', 'datatype', 'tech', 'accuracy']]
        df = cfg.layer_table
        pprint(df)
    except:
        print('  ...no layers defined.')

def show_layercolors():
    """Print the layercolor table."""
    #print('-------------------------------')
    #print("DataFrame 'cfg.layer_table' with layer definitions:")
    print("layercolors:")
    df = cfg.colors[['fill_color', 'frame_color', 'width']]
    pprint(df)


def show_xsection_layer_map():
    """Print the xsection_layer_map table to stdout.

    Returns:
        None
    """
    #print('-------------------------------')
    print("xsection-layer_map:")
    if not cfg.xsection_layer_map.empty:
        df = cfg.xsection_layer_map[['xsection', 'layer_name', 'growx', 'growy']]
        pprint(df)
        #return df
    else:
        print('No xs defined.')


def show_mask_layers():
    """Print mask_layers dictionary with layer export definitions to stdout.

    Returns:
        None
    """
    #print('-------------------------------')
    #print("Dictionary of DataFrames 'cfg.xs_layers' with the description of each xs:")
    columns = ['layer_name', 'layer', 'datatype', 'tech', 'growx', 'growy', 'accuracy']
    print("mask_layers:")
    for xs in cfg.xs_list:
        #for sorted(cfg.mask_layers.keys()):
        under = '_'*(29-len(xs))
        print("xsection: '{}' {}".format(xs, under))
        pprint(get_xsection(xs).mask_layers.reset_index()[columns])
        print('')




